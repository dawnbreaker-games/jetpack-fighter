using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class Stroke3D : Shape3D
{
	public Vector3 startPoint;
	public Vector3 startDirection;
	public ControlPoint[] controlPoints = new ControlPoint[0];
	public AnimationCurve turnRateCurve;
	public AnimationCurve widthCurve;
	public float length;
	public float sampleInterval;
	public int cornersPerRing;

	public Stroke3D ()
	{
	}

	public Stroke3D (Vector3 startPoint, Vector3 startDirection, ControlPoint[] controlPoints, AnimationCurve turnRateCurve, AnimationCurve widthCurve, float length, float sampleInterval, int cornersPerRing)
	{
		this.startPoint = startPoint;
		this.startDirection = startDirection;
		this.controlPoints = controlPoints;
		this.turnRateCurve = turnRateCurve;
		this.widthCurve = widthCurve;
		this.length = length;
		this.sampleInterval = sampleInterval;
		this.cornersPerRing = cornersPerRing;
		SetCornersAndEdgesAndFaces ();
	}

	public virtual void SetCornersAndEdgesAndFaces ()
	{
		SetCorners ();
		SetFacesFromCorners ();
		SetEdgesFromFaces ();
	}

	public override Mesh ToMesh ()
	{
		Mesh output = new Mesh();
		Vector3[] vertices = new Vector3[corners.Length + 2];
		vertices[0] = startPoint;
		Vector3[] cornersInLastRing = new Vector3[cornersPerRing];
		for (int i = 0; i < cornersPerRing; i ++)
		{
			Vector3 corner = corners[corners.Length - 1 - i];
			cornersInLastRing[i] = corner;
		}
		corners.CopyTo(vertices, 1);
		vertices[vertices.Length - 1] = VectorExtensions.GetAverage(cornersInLastRing);
		output.vertices = vertices;
		List<int> triangles = new List<int>();
		int previousIndex = cornersPerRing;
		for (int i = 1; i <= cornersPerRing; i ++)
		{
			triangles.AddRange(new int[] { previousIndex, 0, i });
			previousIndex = i;
		}
		int[] previousCornersInRing = new int[cornersPerRing];
		for (int i = 1; i <= corners.Length; i += cornersPerRing)
		{
			int[] cornersInRing = new int[cornersPerRing];
			for (int i2 = i; i2 < i + cornersPerRing; i2 ++)
				cornersInRing[i2 - i] = i2;
			int previousCornerInRing = cornersInRing[cornersPerRing - 1];
			int previousCornerInPreviousRing = previousCornersInRing[cornersPerRing - 1];
			for (int i2 = 0; i2 < cornersPerRing; i2 ++)
			{
				int cornerInRing = cornersInRing[i2];
				int cornerInPreviousRing = previousCornersInRing[i2];
				triangles.AddRange(new int[] { previousCornerInPreviousRing, cornerInPreviousRing, cornerInRing });
				triangles.AddRange(new int[] { cornerInRing, previousCornerInRing, previousCornerInPreviousRing });
				previousCornerInRing = cornerInRing;
				previousCornerInPreviousRing = cornerInPreviousRing;
			}
			cornersInRing.CopyTo(previousCornersInRing, 0);
		}
		previousIndex = corners.Length;
		for (int i = corners.Length - cornersPerRing; i <= corners.Length; i ++)
		{
			triangles.AddRange(new int[] { previousIndex, i, corners.Length });
			previousIndex = i;
		}
		output.triangles = triangles.ToArray();
		output.RecalculateNormals();
		output.Optimize();
		return output;
	}

	public virtual void SetCorners ()
	{
		float lengthTraveled = 0;
		float normalizedLengthTraveled = lengthTraveled / length;
		List<Vector3> corners = new List<Vector3>();
		Vector3 forward = startDirection.normalized;
		Vector3 position = startPoint;
		int controlPointIndex = 0;
		ControlPoint controlPoint = controlPoints[0];
		ControlPoint nextControlPoint = null;
		if (controlPoints.Length > 1)
			nextControlPoint = controlPoints[1];
		else
			controlPoint = null;
		while (true)
		{
			float radius = widthCurve.Evaluate(normalizedLengthTraveled) / 2;
			float turnRate = turnRateCurve.Evaluate(normalizedLengthTraveled);
			Vector3 tangent = Vector3.up;
			Vector3.OrthoNormalize(ref forward, ref tangent);
			for (int i = 0; i < cornersPerRing; i ++)
			{
				Vector3 corner = position + tangent * radius;
				corner = corner.Rotate(position, Quaternion.AngleAxis(360f / cornersPerRing * i, forward));
				corners.Add(corner);
			}
			forward = Vector3.RotateTowards(forward, controlPoint.point - position, turnRate * sampleInterval * Mathf.Deg2Rad, 0);
			lengthTraveled += sampleInterval;
			normalizedLengthTraveled = lengthTraveled / length;
			if (nextControlPoint != null && normalizedLengthTraveled >= nextControlPoint.startUsingAtNormalizedLength)
			{
				controlPoint = nextControlPoint;
				controlPointIndex ++;
				if (controlPoints.Length > controlPointIndex)
					nextControlPoint = controlPoints[controlPointIndex];
			}
			if (lengthTraveled > length)
			{
				if (lengthTraveled >= length + sampleInterval)
					break;
				position += forward * (lengthTraveled - length - sampleInterval);
				lengthTraveled = length;
				normalizedLengthTraveled = 1;
			}
			else
				position += forward * sampleInterval;
		}
		this.corners = corners.ToArray();
	}

	public void SetFacesFromCorners ()
	{
		List<Face> faces = new List<Face>();
		Vector3[] previousCornersInRing = null;
		for (int i = 0; i < corners.Length; i += cornersPerRing)
		{
			Vector3[] cornersInRing = new Vector3[cornersPerRing];
			for (int i2 = i; i2 < i + cornersPerRing; i2 ++)
				cornersInRing[i2 - i] = corners[i2];
			if (previousCornersInRing == null)
			{
				previousCornersInRing = new Vector3[cornersPerRing];
				faces.Add(new Face(cornersInRing));
			}
			else
			{
				Vector3 previousCornerInRing = cornersInRing[cornersPerRing - 1];
				Vector3 previousCornerInPreviousRing = previousCornersInRing[cornersPerRing - 1];
				for (int i2 = 0; i2 < cornersPerRing; i2 ++)
				{
					Vector3 cornerInRing = cornersInRing[i2];
					Vector3 cornerInPreviousRing = previousCornersInRing[i2];
					Face face = new Face(new Vector3[] { cornerInRing, cornerInPreviousRing, previousCornerInPreviousRing, previousCornerInRing });
					faces.Add(face);
					previousCornerInRing = cornerInRing;
					previousCornerInPreviousRing = cornerInPreviousRing;
				}
			}
			cornersInRing.CopyTo(previousCornersInRing, 0);
		}
		faces.Add(new Face(previousCornersInRing));
		this.faces = faces.ToArray();
	}

	public Stroke3D Move (Vector3 movement)
	{
		Stroke3D output = new Stroke3D();
		output.corners = new Vector3[corners.Length];
		for (int i = 0; i < corners.Length; i ++)
			output.corners[i] = corners[i] + movement;
		output.SetFacesFromCorners ();
		output.SetEdgesFromFaces ();
		output.startPoint += movement;
		output.widthCurve = widthCurve;
		output.turnRateCurve = turnRateCurve;
		output.length = length;
		output.sampleInterval = sampleInterval;
		output.cornersPerRing = cornersPerRing;
		output.controlPoints = new ControlPoint[controlPoints.Length];
		for (int i = 0; i < controlPoints.Length; i ++)
		{
			ControlPoint controlPoint = controlPoints[i];
			controlPoint.point += movement;
			output.controlPoints[i] = controlPoint;
		}
		return output;
	}

	[Serializable]
	public class ControlPoint
	{
		public Vector3 point;
		[Range(0, 1)]
		public float startUsingAtNormalizedLength;
	}
}