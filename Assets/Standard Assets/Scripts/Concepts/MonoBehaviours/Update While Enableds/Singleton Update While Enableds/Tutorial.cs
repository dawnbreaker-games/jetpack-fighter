using UnityEngine;
using UnityEngine.Playables;

namespace HeroesOfSlimeWorld
{
	public class Tutorial : SingletonUpdateWhileEnabled<Tutorial>
	{
		public PlayableDirector playableDirector;
		public GameObject[] activateOnFinish;

		public virtual void Finish ()
		{
			gameObject.SetActive(false);
			for (int i = 0; i < activateOnFinish.Length; i ++)
			{
				GameObject go = activateOnFinish[i];
				go.SetActive(true);
			}
		}
	}
}