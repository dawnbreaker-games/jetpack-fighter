using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[Serializable]
public class Shape2D
{
	public Vector2[] corners;
	public LineSegment2D[] edges;

	public Shape2D ()
	{
	}

	public Shape2D (params Vector2[] corners)
	{
		this.corners = corners;
		SetEdgesOfPolygon ();
	}

	public Shape2D (params LineSegment2D[] edges)
	{
		this.edges = edges;
		SetCornersOfPolygon_Fast ();
	}

#if UNITY_EDITOR
	public virtual void DrawGizmos (Color color)
	{
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			edge.DrawGizmos (color);
		}
	}
#endif

	public virtual void SetCornersOfPolygon ()
	{
		List<Vector2> corners = new List<Vector2>();
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			if (corners.Contains(edge.start))
				corners.Add(edge.end);
			else
				corners.Add(edge.start);
		}
		this.corners = corners.ToArray();
	}

	public virtual void SetCornersOfPolygon_Fast ()
	{
		corners = new Vector2[edges.Length];
		for (int i = 0; i < edges.Length; i ++)
			corners[i] = edges[i].end;
	}

	public virtual void SetEdgesOfPolygon ()
	{
		edges = new LineSegment2D[corners.Length];
		Vector3 previousCorner = corners[corners.Length - 1];
		for (int i = 0; i < corners.Length; i ++)
		{
			Vector2 corner = corners[i];
			edges[i] = new LineSegment2D(previousCorner, corner);
			previousCorner = corner;
		}
	}

	public virtual float GetPerimeter ()
	{
		float output = 0;
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			output += edge.GetLength();
		}
		return output;
	}

	public virtual Vector2 GetPointOnPerimeter (float distance)
	{
		float perimeter = GetPerimeter();
		while (true)
		{
			for (int i = 0; i < edges.Length; i ++)
			{
				LineSegment2D edge = edges[i];
				float edgeLength = edge.GetLength();
				distance -= edgeLength;
				if (distance <= 0)
					return edge.GetPointWithDirectedDistance(edgeLength + distance);
			}
		}
	}

	public virtual bool ContainsForPolygon (Vector2 point, bool equalPointsIntersect = true, float checkDistance = 99999)
	{
		LineSegment2D checkLineSegment = new LineSegment2D(point, point + (Random.insideUnitCircle.normalized * checkDistance));
		int collisionCount = 0;
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			if (edge.DoIIntersectWith(checkLineSegment, equalPointsIntersect))
				collisionCount ++;
		}
		return collisionCount % 2 == 1;
	}

	public virtual bool IsPolygon ()
	{
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			for (int i2 = i + 1; i2 < edges.Length; i2 ++)
			{
				LineSegment2D edge2 = edges[i2];
				if (edge.DoIIntersectWith(edge2, false))
					return false;
			}
		}
		return true;
	}

	public virtual bool DoIIntersectWith (LineSegment2D lineSegment, bool equalPointsIntersect = true)
	{
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			if (edge.DoIIntersectWith(lineSegment, equalPointsIntersect))
				return true;
		}
		return false;
	}

	public virtual Vector2 GetRandomPoint (bool checkIfContained = true, bool containsEdges = true, float checkDistance = 99999)
	{
		float perimeter = GetPerimeter();
		while (true)
		{
			Vector2 point1 = GetPointOnPerimeter(Random.Range(0, perimeter));
			Vector2 point2 = GetPointOnPerimeter(Random.Range(0, perimeter));
			Vector2 output = (point1 + point2) / 2;
			if (!checkIfContained || ContainsForPolygon(output, containsEdges, checkDistance))
				return output;
		}
	}

	public Vector2 GetClosestPoint (Vector2 point, float checkDistance = 99999)
	{
		(Vector2 point, float distanceSqr) closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, checkDistance);
		return closestPointAndDistanceSqr.point;
	}

	public float GetDistanceSqr (Vector2 point, float checkDistance = 99999)
	{
		(Vector2 point, float distanceSqr) closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, checkDistance);
		return closestPointAndDistanceSqr.distanceSqr;
	}

	public (Vector2, float) GetClosestPointAndDistanceSqr (Vector2 point, float checkDistance = 99999)
	{
		if (ContainsForPolygon(point, checkDistance: checkDistance))
			return (point, 0);
		else
		{
			Vector2 closestPoint = new Vector2();
			float closestDistanceSqr = Mathf.Infinity;
			float distanceSqr = 0;
			for (int i = 0; i < edges.Length; i ++)
			{
				LineSegment2D edge = edges[i];
				Vector2 pointOnPerimeter = edge.GetClosestPoint(point);
				distanceSqr = (point - pointOnPerimeter).sqrMagnitude;
				if (distanceSqr < closestDistanceSqr)
				{
					closestDistanceSqr = distanceSqr;
					closestPoint = pointOnPerimeter;
				}
			}
			return (closestPoint, closestDistanceSqr);
		}
	}

	public bool DoIIntersectWithPolygon (Shape2D shape, bool equalPointsIntersect = true, float checkDistance = 99999)
	{
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			if (shape.DoIIntersectWith(edge, equalPointsIntersect))
				return true;
		}
		return ContainsForPolygon(corners[0], equalPointsIntersect, checkDistance);
	}

	public Shape2D Subdivide ()
	{
		List<LineSegment2D> output = new List<LineSegment2D>();
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			output.Add(new LineSegment2D(edge.start, edge.GetMidpoint()));
			output.Add(new LineSegment2D(edge.GetMidpoint(), edge.end));
		}
		return Polygon(output.ToArray());
	}

	public Shape2D Combine (Shape2D shape)
	{
		throw new NotImplementedException();
	}

	public Shape2D IntersectionOfConvexPolygonWithOther_Fast (Shape2D shape, float checkDistance = 99999)
	{
		List<Vector2> corners = new List<Vector2>();
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			for (int i2 = 0; i2 < shape.edges.Length; i2 ++)
			{
				LineSegment2D edge2 = shape.edges[i2];
				Vector2 intersectionPoint;
				if (edge.GetIntersectionWith(edge2, out intersectionPoint, true) && !corners.Contains(intersectionPoint))
					corners.Add(intersectionPoint);
			}
		}
		if (corners.Count == 0)
		{
			if (ContainsForPolygon(shape.corners[0], true, checkDistance))
				return Polygon_Fast(shape.edges);
			else if (shape.ContainsForPolygon(this.corners[0], true, checkDistance))
				return Polygon_Fast(edges);
		}
		else
		{
			for (int i = 0; i < this.corners.Length; i ++)
			{
				Vector2 corner = this.corners[i];
				if (shape.ContainsForPolygon(corner, true, checkDistance) && !corners.Contains(corner))
					corners.Add(corner);
			}
			for (int i = 0; i < shape.corners.Length; i ++)
			{
				Vector2 corner = shape.corners[i];
				if (ContainsForPolygon(corner, true, checkDistance) && !corners.Contains(corner))
					corners.Add(corner);
			}
			List<List<Vector2>> uniquePermutations = corners.ToArray().GetUniquePermutations();
			for (int i = 0; i < uniquePermutations.Count; i ++)
			{
				List<Vector2> uniquePermutation = uniquePermutations[i];
				Shape2D output = Polygon(uniquePermutation.ToArray());
				if (output.IsPolygon())
					return output;
			}
		}
		return null;
	}

	public Shape2D BooleanForPolygon (Shape2D ouptutCanOnlyBeInsideMe)
	{
		throw new NotImplementedException();
	}

	public Shape2D DualCounterpart ()
	{
		throw new NotImplementedException();
	}

	public static Shape2D Polygon (params LineSegment2D[] edges)
	{
		Shape2D output = new Shape2D();
		output.edges = edges;
		output.SetCornersOfPolygon ();
		return output;
	}

	public static Shape2D Polygon_Fast (params LineSegment2D[] edges)
	{
		Shape2D output = new Shape2D();
		output.edges = edges;
		output.SetCornersOfPolygon_Fast ();
		return output;
	}

	public static Shape2D Polygon (params Vector2[] corners)
	{
		Shape2D output = new Shape2D();
		output.corners = corners;
		output.SetEdgesOfPolygon ();
		return output;
	}
}