using UnityEngine;
using System;

namespace HeroesOfSlimeWorld
{
	public class AccountManager : SingletonMonoBehaviour<AccountManager>
	{
		public static Account CurrentAccount
		{
			get
			{
				return Instance.accounts[currentAccountIndex];
			}
			set
			{
				Instance.accounts[currentAccountIndex] = value;
			}
		}
		public static int currentAccountIndex;
		public Account[] accounts = new Account[0];

		[Serializable]
		public class Account
		{
			public string name;
			public string password;
		}
	}
}