﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class InputManager : SingletonMonoBehaviour<InputManager>
	{
		public InputDevice inputDevice;
		public InputSettings settings;
		public static bool UsingGamepad
		{
			get
			{
				return Gamepad.current != null;
			}
		}
		public static bool UsingMouse
		{
			get
			{
				return Mouse.current != null;
			}
		}
		public static bool UsingKeyboard
		{
			get
			{
				return Keyboard.current != null;
			}
		}
		public static bool LeftClickInput
		{
			get
			{
				if (UsingMouse)
					return Mouse.current.leftButton.isPressed;
				else
					return false;
			}
		}
		public static bool RightClickInput
		{
			get
			{
				if (UsingMouse)
					return Mouse.current.rightButton.isPressed;
				else
					return false;
			}
		}
		public static bool LeftTriggerInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.leftTrigger.isPressed;
				else
					return false;
			}
		}
		public static bool RightTriggerInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.rightTrigger.isPressed;
				else
					return false;
			}
		}
		public static bool LeftShoulderInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.leftShoulder.isPressed;
				else
					return false;
			}
		}
		public static bool RightShoulderInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.rightShoulder.isPressed;
				else
					return false;
			}
		}
		public static bool RightStickButtonInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.rightStickButton.isPressed;
				else
					return false;
			}
		}
		public static Vector2? MousePosition
		{
			get
			{
				if (UsingMouse)
					return Mouse.current.position.ReadValue();
				else
					return null;
			}
		}
		public static Vector2? LeftStickInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.leftStick.ReadValue();
				else
					return null;
			}
		}
		public static Vector2? RightStickInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.rightStick.ReadValue();
				else
					return null;
			}
		}
		public static bool MenuInput
		{
			get
			{
				return Keyboard.current.escapeKey.isPressed;
			}
		}

		public static float GetAxis (InputControl<float> positiveButton, InputControl<float> negativeButton)
		{
			return positiveButton.ReadValue() - negativeButton.ReadValue();
		}

		public static Vector2 GetAxis2D (InputControl<float> positiveXButton, InputControl<float> negativeXButton, InputControl<float> positiveYButton, InputControl<float> negativeYButton)
		{
			Vector2 output = new Vector2();
			output.x = positiveXButton.ReadValue() - negativeXButton.ReadValue();
			output.y = positiveYButton.ReadValue() - negativeYButton.ReadValue();
			output = Vector2.ClampMagnitude(output, 1);
			return output;
		}
		
		public enum InputDevice
		{
			KeyboardAndMouse,
			Gamepad
		}
	}
}