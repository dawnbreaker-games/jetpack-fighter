#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace HeroesOfSlimeWorld
{
	public class SetInputDevice : EditorScript
	{
		const string PATH_TO_INPUT_MANAGER = "Assets/Prefabs/Managers (Prefabs)/Input Manager.prefab";

		[MenuItem("Game/Use keyboard and mouse")]
		static void SetToKeyboardAndMouse ()
		{
			InputManager inputManager = (InputManager) AssetDatabase.LoadAssetAtPath(PATH_TO_INPUT_MANAGER, typeof(InputManager));
			inputManager.inputDevice = InputManager.InputDevice.KeyboardAndMouse;
			PrefabUtility.SavePrefabAsset(inputManager.gameObject);
		}

		[MenuItem("Game/Use gamepad")]
		static void SetToGamepad ()
		{
			InputManager inputManager = (InputManager) AssetDatabase.LoadAssetAtPath(PATH_TO_INPUT_MANAGER, typeof(InputManager));
			inputManager.inputDevice = InputManager.InputDevice.Gamepad;
			PrefabUtility.SavePrefabAsset(inputManager.gameObject);
		}
	}
}
#else
namespace HeroesOfSlimeWorld
{
	public class SetInputDevice : EditorScript
	{
	}
}
#endif