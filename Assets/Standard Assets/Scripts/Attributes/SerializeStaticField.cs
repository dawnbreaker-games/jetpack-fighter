﻿#if UNITY_EDITOR
using System;
using System.IO;
using Extensions;
using UnityEngine;
using UnityEditor;
using HeroesOfSlimeWorld;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class SerializeStaticField : _Attribute
{
	public override void Do ()
	{
		MonoScript monoScript = MonoScript.FromMonoBehaviour(targetObject as MonoBehaviour);
		string text = monoScript.text;
		int indexOfMemberName = text.IndexOf(targetName);
		int indexOfNewLineAfterMemberName = text.IndexOf("\n", indexOfMemberName);
		text = text.Insert(indexOfNewLineAfterMemberName + 1, "public " + targetType.Name +  " _" + targetName + ";\n");
		string startBraceIndicator = "{";
		string initialAssignmentLine = targetName + " = _" + targetName + ";\n";
		string awakeIndicator = "void Awake ()";
		int indexOfAwakeIndicator = text.IndexOf(awakeIndicator);
		if (indexOfAwakeIndicator != -1)
		{
			int indexOfStartBraceIndicatorAfterAwakeIndicator = text.IndexOf(startBraceIndicator, indexOfAwakeIndicator);
			GameManager.Log (indexOfStartBraceIndicatorAfterAwakeIndicator);
			text = text.Insert(indexOfStartBraceIndicatorAfterAwakeIndicator + startBraceIndicator.Length, initialAssignmentLine);
		}
		else
		{
			int indexOfClassIndicator = text.IndexOf("class");
			GameManager.Log (indexOfClassIndicator);
			int indexOfStartBraceIndicatorAfterClassIndicator = text.IndexOf(startBraceIndicator, indexOfClassIndicator);
			GameManager.Log (indexOfStartBraceIndicatorAfterClassIndicator);
			text = text.Insert(indexOfStartBraceIndicatorAfterClassIndicator + startBraceIndicator.Length, "void Awake ()\n{\n" + initialAssignmentLine + "}\n");
		}
		string path = AssetDatabase.GetAssetPath(monoScript);
		File.WriteAllText(path, text);
	}
}
#else
using System;
using HeroesOfSlimeWorld;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class SerializeStaticField : _Attribute
{
}
#endif