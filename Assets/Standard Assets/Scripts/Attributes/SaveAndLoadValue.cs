﻿using System;
using System.Collections;
using System.Collections.Generic;

[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Class)]
public class SaveAndLoadValueAttribute : Attribute
{
}
