using System;
using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace HeroesOfSlimeWorld
{
	public class Player : Acter, IUpdatable, IDestructable
	{
		public float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public float maxHp;
		public float MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public new Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}
		public Transform groundCheckTrs;
		public LayerMask whatIsWall;
		public float runSpeed;
		public float jumpSpeed;
		public float flySpeed;
		public Rigidbody2D rigid;
		public LayerMask whatKillsMe;
		public float stopDistance;
		public float flyDurationRegenDelay;
		public float flyDurationRegenRate;
		public float maxFlyDuration;
		public Transform flyDurationIndicatorTrs;
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>();
				return instance;
			}
		}
		float flyDuration;
		float timeTillFlyDurationRegen;

		void OnEnable ()
		{
			instance = this;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (timeTillFlyDurationRegen <= 0)
				flyDuration = Mathf.Clamp(flyDuration + flyDurationRegenRate * Time.deltaTime, 0, maxFlyDuration);
			else
				timeTillFlyDurationRegen -= Time.deltaTime;
			HandleMoving ();
			flyDurationIndicatorTrs.localScale = flyDurationIndicatorTrs.localScale.SetX(flyDuration / maxFlyDuration);
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		void HandleMoving ()
		{
			Vector2 move = new Vector2();
			if (Keyboard.current.dKey.isPressed)
				move.x ++;
			if (Keyboard.current.aKey.isPressed)
				move.x --;
			if (Keyboard.current.wKey.isPressed)
				move.y ++;
			if (Keyboard.current.sKey.isPressed)
				move.y --;
			if (move != Vector2.zero)
				MoveToward ((Vector2) trs.position + move);
			else
				rigid.velocity = rigid.velocity.SetX(0);
		}

		void MoveToward (Vector2 position)
		{
			Vector2 desiriedMove = position - (Vector2) trs.position;
			Vector2 move = desiriedMove.normalized * 99999;
			Vector2 groundMovement = Vector2.right * move.x;
			groundMovement = Vector2.ClampMagnitude(groundMovement, runSpeed);
			if (Physics2D.Raycast(groundCheckTrs.position + trs.up * Physics.defaultContactOffset * 2, -trs.up, Physics.defaultContactOffset * 4, whatIsWall).collider != null)
			{
				if (move.y > 0)
					move = groundMovement.SetY(jumpSpeed);
				else
					move = groundMovement.SetY(rigid.velocity.y);
			}
			else if (flyDuration > 0)
			{
				move = groundMovement.SetY(rigid.velocity.y);
				if (desiriedMove.y != 0 || Keyboard.current.spaceKey.isPressed)
				{
					flyDuration -= Time.deltaTime;
					timeTillFlyDurationRegen = flyDurationRegenDelay;
					move += desiriedMove.normalized * flySpeed * Time.deltaTime;
				}
			}
			else
				move = rigid.velocity;
			if (desiriedMove.SetY(0).sqrMagnitude <= stopDistance * stopDistance)
			{
				trs.position += Vector3.right * desiriedMove.x;
				move.x = 0;
			}
			rigid.velocity = move;
		}

		public override void Act ()
		{
		}

		public void TakeDamage (float amount)
		{
			hp -= amount;
			if (hp <= 0)
				Death ();
		}

		public void Death ()
		{
			if (isDead)
				return;
			isDead = true;
			_SceneManager.instance.RestartScene ();
		}
		
		public override void InitData ()
		{
			if (data == null)
				data = new Data();
		}

		public override void SetData ()
		{
			InitData ();
			base.SetData ();
		}

		[Serializable]
		public class Data : Acter.Data
		{
			public override object MakeAsset ()
			{
				Player player = Instantiate(GameManager.instance.playerPrefab, GameManager.gameStateGos[GameManager.gameStateGos.Count - 1].GetComponent<Transform>());
				Apply (player);
				return player;
			}

			public override void Apply (Asset asset)
			{
				if (asset.data == null)
					asset.data = this;
				base.Apply (asset);
			}
		}
	}
}